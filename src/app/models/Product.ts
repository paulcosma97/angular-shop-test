export class Product {
  id: number;
  name: string;
  description: string;
  price_ron: number;
}
