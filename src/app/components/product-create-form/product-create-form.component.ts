import { Component, OnInit } from '@angular/core';
import {Product} from '../../models/Product';
import {DataService} from '../services/data/data.service';

@Component({
  selector: 'app-product-create-form',
  templateUrl: './product-create-form.component.html',
  styleUrls: ['./product-create-form.component.css']
})
export class ProductCreateFormComponent implements OnInit {
  product: Product = {
    id: 0,
    name: '',
    description: '',
    price_ron: 0,
  };
  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  appendProduct(product: Product) {
    this.dataService.addProduct(product);
    this.product = {
      id: 0,
      name: '',
      description: '',
      price_ron: 0,
    };
  }
}
