import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Product } from '../../models/Product';
import {DataService} from '../services/data/data.service';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {
  constructor(private dataService: DataService) {
  }
  @Input() selectedProduct: Product;
  @Output() selectProductEvent = new EventEmitter<Product>();
  products: Product[];

  calculatePrice(price_ron: number) {
    return (price_ron * 4.66).toFixed(2);
  }

  ngOnInit() {
    this.dataService.products.subscribe(products => this.products = products);
    this.products = this.dataService.getAllProducts();
  }
  onSelect(product: Product) {
    this.selectProductEvent.emit(product);
  }
}
