import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data/data.service';
import {Product} from '../../models/Product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Shop Sandbox';
  products: Product[];
  selectedProduct: Product;
  updateFormShown = true;
  constructor(private dataService: DataService) {
  }
  ngOnInit() {
    this.dataService.products.subscribe(products => this.products = products);
    this.products = this.dataService.getAllProducts();
    this.selectedProduct = this.products[0];
  }

  selectProduct($event: Product) {
    this.selectedProduct = $event;
  }

  showUpdateForm() {
    this.updateFormShown = true;
  }

  showCreateForm() {
    this.updateFormShown = false;
  }
}
