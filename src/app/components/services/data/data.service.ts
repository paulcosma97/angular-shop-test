import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Product} from '../../../models/Product';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private productSource = new BehaviorSubject([
    {id: 1, name: 'test', price_ron: 500, description: 'mockt selectedProduct'},
    {id: 2, name: 'test1', price_ron: 520, description: 'mockt selectedProduct 2'},
    {id: 3, name: 'test2', price_ron: 540, description: 'mockt selectedProduct 3'},
    {id: 4, name: 'test3', price_ron: 560, description: 'mockt selectedProduct 4'}
  ]);
  products = this.productSource.asObservable();
  constructor() { }
  getAllProducts() {
    return this.productSource.getValue();
  }
  addProduct(product: Product) {
    const products = this.productSource.getValue();
    let maxId = products[0].id;
    products.forEach(p => {
      if (p.id > maxId) {
        maxId = p.id;
      }
    });
    product.id = maxId + 1;
    products.push(product);
    this.productSource.next(products);
  }
}
