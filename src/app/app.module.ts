import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './components/app/app.component';
import { ProductTableComponent } from './components/product-table/product-table.component';
import { ProductUpdateFormComponent } from './components/product-update-form/product-update-form.component';
import { ProductCreateFormComponent } from './components/product-create-form/product-create-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductTableComponent,
    ProductUpdateFormComponent,
    ProductCreateFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
